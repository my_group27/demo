package com.demo.custom_elements;

import com.demo.util.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.stream.Collectors;

public class Table {

    private WebElement webElement;

    public Table(WebElement webElement) {
        this.webElement = webElement;
    }

    public List<String> getColumnData(int index) {
        List<WebElement> columnCells = webElement.findElements(By.xpath(String.format(".//td[%s]", (index + 1))));
        return columnCells.stream().map(WebElement::getText).collect(Collectors.toList());
    }

}
