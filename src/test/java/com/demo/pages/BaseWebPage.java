package com.demo.pages;

import com.demo.util.WebDriverProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BaseWebPage {

    private WebDriver driver;

    public BaseWebPage() {
        driver = WebDriverProvider.getDriver();
        PageFactory.initElements(driver, this);
    }

}
