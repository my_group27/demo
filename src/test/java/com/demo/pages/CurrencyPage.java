package com.demo.pages;

import com.demo.custom_elements.Table;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CurrencyPage extends BaseWebPage {

    @FindBy(className = "referenceable")
    WebElement title;

    @FindBy(className = "data")
    WebElement table;

    private Table tableImpl() {
        return new Table(table);
    }

    public Map<String, String> getCurrencies() {
        Table table = tableImpl();
        List<String> currencyCodes = table.getColumnData(1);
        List<String> currencyValues = table.getColumnData(4);
        Assert.assertEquals(currencyCodes.size(), currencyValues.size(),
                "Currency codes count should match currency values count");
        Map<String, String> result = new LinkedHashMap<>();
        for (int i = 0; i < currencyCodes.size(); i++) {
            result.put(currencyCodes.get(i), currencyValues.get(i));
        }
        return result;
    }

}
