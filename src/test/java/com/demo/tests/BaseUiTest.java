package com.demo.tests;

import com.demo.util.WebDriverProvider;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseUiTest {

    @BeforeMethod
    public void createDriver() {
        WebDriverManager.chromedriver()
                .setup();
        WebDriver driver = new ChromeDriver(new ChromeOptions());
        WebDriverProvider.setDriver(driver);
    }

    @AfterMethod
    public void cleanUp() {
        WebDriver driver = WebDriverProvider.getDriver();
        if (driver != null)
            driver.quit();
    }

    protected void navigateTo(String url) {
        WebDriverProvider.getDriver().get(url);
    }
}
