package com.demo.tests;

import com.demo.pages.CurrencyPage;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Map;

public class DemoTest extends BaseUiTest {

    private void createXml(Map<String, String> currencies) throws Exception {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("exchangeRate");
        doc.appendChild(rootElement);

        for (Map.Entry<String, String> currency : currencies.entrySet()) {
            Element node = doc.createElement("add");
            node.setAttribute("key", currency.getKey());
            node.setAttribute("value", currency.getValue());
            rootElement.appendChild(node);
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(
                new File("src" + File.separator + "main" + File.separator + "resources" + File.separator + "currencies.xml"));
        transformer.transform(source, result);
    }

    @Test
    public void demoTest() throws Exception {
        navigateTo("https://cbr.ru/currency_base/daily/");
        CurrencyPage currencyPage = new CurrencyPage();
        Map<String, String> currencies = currencyPage.getCurrencies();
        createXml(currencies);
    }

}
